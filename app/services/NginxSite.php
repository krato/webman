<?php

namespace App\Services;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class NginxSite extends \Nette\Object {

	public $url;
	public $root;
	public $url_aliases = [];
	public $includes = [];

	public function toString() {
		$server_name = trim(sprintf('%s %s', $this->url, implode(' ', $this->url_aliases)));
		$rows = [];
		$rows[] = "server {";
		$rows[] = "\tserver_name $server_name;";
		$rows[] = "\troot $this->root;";
		foreach ($this->includes as $include) {
			$rows[] = "\tinclude include/$include;";
		}
		$rows[] = "}";
		$rows[] = "";
		return implode("\n", $rows);
	}

}
