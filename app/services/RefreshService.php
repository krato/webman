<?php

namespace App\Services;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class RefreshService extends \Nette\Object {

	/** @var array */
	protected $config;

	/** @var \App\Repositories\ProjectRepository */
	protected $projectRepository;

	/** @var \App\Repositories\BranchRepository */
	protected $branchRepository;

	/** @var \App\Repositories\UnixGroupRepository */
	protected $unixGroupRepository;

	/** @var \App\Repositories\ProjectUserRepository */
	protected $projectUserRepository;
	protected static $nginxConfigFilename = 'output/webman';
	protected static $scriptFilename = 'output/webman.sh';

	public function __construct(
	$config
	, \App\Repositories\ProjectRepository $projectRepository
	, \App\Repositories\BranchRepository $branchRepository
	, \App\Repositories\UnixGroupRepository $unixGroupRepository
	, \App\Repositories\ProjectUserRepository $projectUserRepository
	) {
		$this->config = $config;
		assert(isset($this->config['webroot']));
		assert(isset($this->config['url_suffix']));
		$this->projectRepository = $projectRepository;
		$this->branchRepository = $branchRepository;
		$this->unixGroupRepository = $unixGroupRepository;
		$this->projectUserRepository = $projectUserRepository;
	}

	public function refreshAll() {
		if (file_exists(self::$scriptFilename)) {
			unlink(self::$scriptFilename);
		}

		$sites = $this->generateNginxSites();
		file_put_contents(self::$nginxConfigFilename, $sites);

		$script = $this->generatePermissions();
		file_put_contents(self::$scriptFilename, $script);
	}

	protected function generateNginxSites() {
		$result = "";
		$projects = $this->projectRepository->findAll();
		foreach ($projects as $project) {
			$branches = $project->related($this->branchRepository->tableName);
			foreach ($branches as $branch) {
				$site = new \App\Services\NginxSite();
				$root = sprintf('%s/%s/%s', $this->config['webroot'], $project->name, $branch->name);
				if ($project->system == 'nette') {
					$root .= '/www';
				}
				$site->root = $root;
				$site->url = sprintf('%s.%s.%s', $branch->name, $project->name, $this->config['url_suffix']);
				$site->url_aliases = explode(' ', $branch->url_aliases);
				$site->includes['default'] = 'default';
				$protection = $branch->protection;
				if ($protection) {
					$site->includes[] = 'auth-' . $protection;
				}
				switch ($project->system) {
					case 'drupal7':
					case 'drupal8':
					case 'nette':
					case 'index':
					case 'php7':
						$site->includes[] = 'php7';
						break;
					case 'php5':
						$site->includes[] = 'php5';
						break;
					default:
						break;
				}
				switch ($project->system) {
					case 'drupal7':
					case 'drupal8':
					case 'nette':
					case 'index':
						$site->includes[] = $project->system;
				}
				$result .= $site->toString();
			}
		}
		return $result;
	}

	protected function generatePermissions() {
		$script = [];
		$script[] = '#!/bin/bash';
		$script[] = 'set -e';
		$script[] = 'set -x';
		$script[] = '';
		$groupsOld = $this->unixGroupRepository->getAll('web-');
		$groupNamesNew = [];
		$projects = $this->projectRepository->findAll();
		foreach ($projects as $project) {
			$groupNamesNew[] = sprintf('web-%s', $project->name);
		}
		$groupNamesToCreate = [];
		foreach ($groupNamesNew as $groupNameNew) {
			$found = FALSE;
			foreach ($groupsOld as $groupOld) {
				if ($groupOld->name == $groupNameNew) {
					$found = TRUE;
					break;
				}
			}
			if (!$found) {
				$groupNamesToCreate[] = $groupNameNew;
			}
		}
		if ($groupNamesToCreate) {
			foreach ($groupNamesToCreate as $groupNameToCreate) {
				$script[] = sprintf('groupadd %s', $groupNameToCreate);
			}
			$script[] = '';
		}

		$groupNamesToDelete = [];
		foreach ($groupsOld as $groupOld) {
			$found = FALSE;
			foreach ($groupNamesNew as $groupNameNew) {
				if ($groupOld->name == $groupNameNew) {
					$found = TRUE;
					break;
				}
			}
			if (!$found) {
				$groupNamesToDelete[] = $groupOld->name;
			}
		}
		if ($groupNamesToDelete) {
			foreach ($groupNamesToDelete as $groupNameToDelete) {
				$script[] = sprintf('groupdel %s', $groupNameToDelete);
			}
			$script[] = '';
		}

		foreach ($projects as $project) {
			$groupName = sprintf('web-%s', $project->name);
			$path = sprintf('%s/%s', $this->config['webroot'], $project->name);
			$script[] = sprintf('chown -hR :%s %s', $groupName, $path);
			$script[] = sprintf('chmod -R g+srw %s', $path);
			$script[] = sprintf('chmod g+x %s', $path);
			$script[] = sprintf('chmod o-rwx %s', $path);
			$script[] = sprintf('chmod o-w %s/*', $path);
			$script[] = sprintf('chmod o+rx %s/*', $path);
			$script[] = '';
		}

		foreach ($groupsOld as $groupOld) {
			$groupOldName = $groupOld->name;
			foreach ($groupOld->members as $user) {
				$script[] = sprintf('usermod -G %s %s', $groupOldName, $user);
			}
		}
		$script[] = '';

		$user = 'www-data';
		foreach ($projects as $project) {
			$groupName = sprintf('web-%s', $project->name);
			$script[] = sprintf('usermod -a -G %s %s', $groupName, $user);
		}
		$script[] = '';

		foreach ($projects as $project) {
			$groupName = sprintf('web-%s', $project->name);
			$userNames = $this->projectUserRepository->getUserNamesByProjectName($project->name);
			var_dump($userNames);
			if ($userNames) {
				foreach ($userNames as $user) {
					$script[] = sprintf('usermod -a -G %s %s', $groupName, $user);
				}
			}
		}
		$script[] = '';

		return implode("\n", $script) . "\n";
	}

}
