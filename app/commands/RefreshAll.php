<?php

namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshAll extends Command {

	/** @var \App\Services\RefreshService @inject */
	public $refreshService;

	protected function configure() {
		$this->setName('refresh:all')->setDescription('Refresh system to reflect database.');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		try {
			$output->write('Creating output files ... ');
			
			$this->refreshService->refreshAll();

			$output->writeLn('<info>[OK]</info>');
			return 0; // zero return code means everything is ok
		} catch (\Exception $exc) {
			$output->writeLn('<error>' . $exc->getMessage() . '</error>');
			return 1; // non-zero return code means error
		}
	}

}
