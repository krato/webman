<?php

namespace App\Repositories;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class ProjectUserRepository extends BaseDatabaseRepository {

	public function getUserNamesByProjectName($projectName) {
		return $this->findAll()->select('user')->where('project', $projectName)->fetchPairs('user', 'user');
	}

}
