<?php

namespace App\Repositories;

/**
 * @property-read string $tableName
 */
abstract class BaseDatabaseRepository extends \Nette\Object
{

    /** @var string */
    protected $tableName;

    /** @var \Nette\Database\Context */
	protected $context;

    public function __construct($tableName, \Nette\Database\Context $context)
    {
        $this->tableName = $tableName;
        $this->context = $context;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->context->table($this->tableName);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findById($id)
    {
        return $this->findAll()->where(['id' => $id]);
    }
    
    /**
     * Insert new row
     * @param array $values
     * @return \Nette\Database\Table\IRow the new row
     */
    public function insert($values)
    {
        return $this->findAll()->insert($values);
    }

    /**
	 * Update existing row
	 * @param array $values
	 * @return int number of affected rows
	 */
	public function update($values)
	{
		return $this->findById($values->id)->update($values);
	}

	/**
	 * Delete existing row
	 * @param \Nette\Database\Table\IRow|int $id
	 * @return int number of affected rows
	 */
	public function delete($id)
	{
		if ($id instanceof \Nette\Database\Table\IRow) {
			$id = $id->id;
		}
		return $this->findById($id)->delete();
	}

}
