<?php

namespace App\Repositories;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class UnixGroupRepository extends \Nette\Object {

	/**
	 * @param string $prefix
	 * @return \App\Repositories\UnixGroup[]
	 */
	public function getAll($prefix = NULL) {
		$groups = [];
		$rows = file('/etc/group');
		foreach ($rows as $row) {
			$cols = explode(':', trim($row));
			$name = $cols[0];
			if ($prefix != NULL) {
				if (substr($name, 0, strlen($prefix)) != $prefix) {
					continue;
				}
			}
			$group = new UnixGroup();
			$group->name = $name;
			$group->id = $cols[2];
			if ($cols[3]) {
				$group->members = explode(',', $cols[3]);
			} else {
				$group->members = [];
			}
			$groups[] = $group;
		}
		return $groups;
	}

}
