<?php

namespace App\Repositories;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class UnixGroup extends \Nette\Object {

	public $id;
	public $name;
	public $members;

}
