#!/bin/bash
ROOT=`dirname "$0"`
cd "$root"
set -e
set -x
rm -rf temp/cache/
php www/index.php refresh:all
cat "output/webman"
cp "output/webman" "/etc/nginx/sites-enabled/webman"
. "output/webman.sh"
/etc/init.d/nginx reload
if [ -e /etc/init.d/samba ]
then
	/etc/init.d/samba restart
fi
